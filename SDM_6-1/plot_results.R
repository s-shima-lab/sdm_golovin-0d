##### functions
besselI1_exponscaled_cont <- function(x) {
# to avoid the overflow of besselI
# for large x, asymptotic form is used

    crt <- 1.0e5
    xrange <- (x<crt)
    y<-rep(1,length(x))
    y[xrange] <- besselI(x[xrange],1, expon.scaled = T)
    y[!xrange] <- 1.0/sqrt(2.0*pi*x[!xrange])

    return(y)
}

##### main program
# Make the list of files
tmp_files = dir("./",pattern="^data_")
tmp = strsplit(tmp_files,"data_|.txt")
alltimes = unique(matrix(unlist(tmp),nrow=2)[2,])

allfiles = tmp_files
names(allfiles) = alltimes

# read parameters
tmp = read.table('log.txt',header=FALSE,fill=TRUE)
num_conc_0 = as.numeric(tmp[tmp$V1=="num_conc_0",3])
lwc = as.numeric(tmp[tmp$V1=="lwc",3])
dvol = as.numeric(tmp[tmp$V1=="dvol",3])
para_b = as.numeric(tmp[tmp$V1=="para_b",3])

# set parameters
rho_liqw <- 1.0E+3 # density of liquid water [kg/m3]

# set parameters for plotting the exact solution
radi_min =   10.0           # [um] maximum radius for plot
radi_max = 5000.0           # [um] minimum radius for plot 
num_grid = 1000             # number of grid points for plot
mass_dens_drop_radi <- exp(seq(log(radi_min),log(radi_max),(log(radi_max)-log(radi_min))/num_grid))

# vector for the number concentration time series
num_conc_t <- rep(0,length(alltimes))
names(num_conc_t) <- alltimes

# open png file for mass density distribution
png("mass_dens_drop.png")

#### loop of time
for(time in alltimes){
    cat(sprintf("processing the time = %s [s]\n",time))

    # read data
    file <- allfiles[time]
    data <- read.table(file,header=T)
    names(data)=list("sd_mass","sd_n","sd_status","index")

    valid_data <- data[data$sd_status,]

    # Mass density distribution of droplets (simulation)
    drop_data <- valid_data
    drop_radi <- (drop_data$sd_mass/(pi*4.0/3.0)/rho_liqw)**(1.0/3.0)
    drop_num  <- sum(drop_data$sd_n)

    num_conc_t[time] <- drop_num/dvol

    total_liqmass <- sum(drop_data$sd_n*drop_data$sd_mass)
    lwc_actual <- total_liqmass/dvol
    #cat(sprintf("lwc_actual = %f\n",lwc_actual))
    
    mass_dens_drop <- density(log(drop_radi),bw="SJ",weight=drop_data$sd_n*drop_data$sd_mass/total_liqmass)
    mass_dens_drop$x <- exp(mass_dens_drop$x)* 1.0e6 # [m]->[um]
    mass_dens_drop$y <- lwc_actual*1.0e3*mass_dens_drop$y

    # Mass density distribution of droplets (exact)
    tmp_drop_mass <- (rho_liqw)*(4.0/3.0)*pi*(mass_dens_drop_radi/1.0e6)**3
    m0=lwc/num_conc_0
    if(as.numeric(time)==0){
        exact_num_dens_drop_vs_mass = (num_conc_0/m0)*exp(-tmp_drop_mass/m0)
    }else{    
        tmp_s <- tmp_drop_mass/m0
    	tmp_tau <- 1.0 - exp(-(para_b*lwc/rho_liqw)*as.numeric(time))
    	tmp <- 2.0*tmp_s*sqrt(tmp_tau)
        exact_num_dens_drop_vs_mass = (num_conc_0/m0)*(1.0-tmp_tau)/(tmp_s*sqrt(tmp_tau))*
    			besselI1_exponscaled_cont(tmp)*exp(-(1.0+tmp_tau)*tmp_s+tmp)
    }
    exact_mass_dens_drop_vs_lnr = 3.0*tmp_drop_mass**2*exact_num_dens_drop_vs_mass*1.0e3 # [kg]->[g]

    # Plot Mass density distribution of droplets
    labels <- c("theory","simulation")
    cols <- c("red","black")
    lwds <- c(2.5,2)
    ltys <- c(1,1)
    #### exact
    id <- 1
    plot(mass_dens_drop_radi,exact_mass_dens_drop_vs_lnr,type="l",col=cols[id],lwd=lwds[id],lty=ltys[id],
        log="x",
	xlim=c(radi_min,radi_max),
	ylim=c(0,1.8),
	main=paste("Mass density distribution of droplets"),
    	xlab="Radius of droplet [um]",
    	ylab="Mass density distribution m(r)dN/dlogr ([g/unit logr/m^3])")
    par(new=T,ann=F)

    ##### simulation
    id <-2
    plot(mass_dens_drop,col=cols[id],lwd=lwds[id],lty=ltys[id],
        log="x",
	xlim=c(radi_min,radi_max),
	ylim=c(0,1.8),
	main=paste("Mass density distribution of droplets"),
    	xlab="Radius of droplet [um]",
    	ylab="Mass density distribution m(r)dN/dlogr ([g/unit logr/m^3])")
    par(new=T,ann=F)

}

legend("topright", legend=labels, col=cols, lwd=lwds, lty=ltys)
dev.off()

# open png file for the time series of number cocentration
png("num_time-series.png")
labels <- c("theory","simulation")
cols <- c("red","black")
lwds <- c(2.5,2)
ltys <- c(1,NA)
pchs <- c(NA,3)
#### exact
id <- 1
time_max <- max(as.numeric(alltimes))
tmp_time <- seq(0,time_max,100)
tmp_num_conc <- num_conc_0*exp(-(para_b*lwc/rho_liqw)*tmp_time)
plot(tmp_time,tmp_num_conc,type="l",col=cols[id],lwd=lwds[id],lty=ltys[id],
        log="y",
	xlim=c(0,time_max),
	ylim=c(2e4,1e7),
	main=paste("Number concentration of droplets"),
    	xlab="Time t [s]",
    	ylab="Number concentration n(t) [/m^3]")
par(new=T,ann=F)

#### simulation
id <- 2
plot(as.numeric(alltimes),num_conc_t,type="p",col=cols[id],lwd=lwds[id],pch=pchs[id],cex=2,
        log="y",
	xlim=c(0,time_max),
	ylim=c(2e4,1e7),
	main=paste("Number concentration of droplets"),
    	xlab="Time t [s]",
    	ylab="Number concentration n(t) [/m^3]")
par(new=T,ann=F)

legend("topright", legend=labels, col=cols, lwd=lwds, lty=ltys, pch=pchs, pt.cex=2)

dev.off()
