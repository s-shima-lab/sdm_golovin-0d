#!/bin/bash
#PBS -q T
#PBS -l select=1:ncpus=40:mpiprocs=0
#PBS -l walltime=00:05:00
#PBS -N sdm-khpcs
#PBS -j oe


source /etc/profile.d/modules.sh
cd ${PBS_O_WORKDIR}
module load intel

## serial run
#./sdm_test-golovin

## serial run with performance monitoring
source /apps/intel/vtune_profiler/amplxe-vars.sh
vtune -collect hotspots ./sdm_test-golovin

## OpenMP setup
#export OMP_NUM_THREADS=4
#export KMP_AFFINITY=disabled

## OpenMP run
#dplace ./sdm_test-golovin

## OpenMP run with performance monitoring
#source /apps/intel/vtune_profiler/amplxe-vars.sh
#vtune -collect hotspots dplace ./sdm_test-golovin
