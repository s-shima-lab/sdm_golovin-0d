## Description  
This is an educational material to learn the Monte Carlo collision-coalescence algorithm of the Super-Droplet Method (SDM; Shima et al. 2009, 2020).
The collision-coalescence of droplets in a well-mixed volume (0D) is simulated.
Golovin kernel is used for the collision-coalescence kernel.

SDM is introduced step by step:

- SDM_3: all-to-all collision-coalescence of real droplets 
- SDM_4: all-to-all collision-coalescence of super-droplets
- SDM_5: candidate pair reduction and multiple coalescence
- SDM_6: multithreading
    - SDM_6-1: OpenMP v1
    - SDM_6-2: OpenMP v2
	- SDM_6-3: SIMD
	
## Software requriements  
- minimal: Intel® Fortran Compiler, R  
- optional: Intel® VTune™ Profiler, PBS, HPE SGI NUMA tools 

## How to use    
- Make  
```
$ module purge
$ module load intel
$ cd Src/
$ vi Makefile
$ make
$ cd ..
```
- Run  
```
$ sh clean_data.sh
$ export OMP_NUM_THREADS=1
$ ./sdm_test-golovin &
$ tail -f log.txt
```
Or, use the batch script
```
$ sh clean_data.sh 
$ qsub go.sh
$ qstat -a
$ tail -f log.txt
```
- Post analysis
```
$ module load R
$ Rscript plot_results.R
$ eog mass_dens_drop.png &
$ eog num_time-series.png &
```

## Performance profiling using Intel® VTune™ Profiler
- Make  
```
$ cd Src/
```
Add "-g" option to the compile option FFLAGS
```
$ vi Makefile
```
```
$ make clean
$ make
$ cd ..
```
- Run  
```
$ sh clean_data.sh
$ export OMP_NUM_THREADS=4
$ source /apps/intel/vtune_profiler/amplxe-vars.sh
$ vtune -collect hotspots ./sdm_test-golovin &
$ tail -f log.txt
```
- Loop/function info  
Hereafter, assume that the profile data is saved in "r000hs".
```
$ vtune -report summary -report-knob show-issues=false -loop-mode=loop-and-function -r r000hs/
```
- Threading info (simple version)
```
$ vtune -report hotspots -group-by thread -column="CPU Time:Self" -column="CPU Time:Effective Time:Self" -column="CPU Time:Spin Time:Self" -r r000hs
```
- Threading info (detailed version)  
All threads
```
$ vtune -report=hotspots -group-by=thread,function -loop-mode=loop-and-function -column="CPU Time:Self" -column="CPU Time:Effective Time:Self" -column="CPU Time:Spin Time:Self"  -S "CPU Time" -r r000hs/
```
Extract thread #0 info
```
$ vtune -report=hotspots -group-by=thread,function -loop-mode=loop-and-function -column="CPU Time:Self" -column="CPU Time:Effective Time:Self" -column="CPU Time:Spin Time:Self"  -S "CPU Time" -r r000hs/ | grep -e \#0 -e "CPU Time"
```
Extract thread #1 info
```
$ vtune -report=hotspots -group-by=thread,function -loop-mode=loop-and-function -column="CPU Time:Self" -column="CPU Time:Effective Time:Self" -column="CPU Time:Spin Time:Self"  -S "CPU Time" -r r000hs/ | grep -e \#1 -e "CPU Time"
```

## References
- Golovin AM. 1963. The solution of the coagulation equation for cloud droplets in a rising air current. Bull. Acad. Sci., USSR, Geophys. Ser. X 5: 783–791.
- Shima, S., Kusano, K., Kawano, A., Sugiyama, T. and Kawahara, S. (2009), The super-droplet method for the numerical simulation of clouds and precipitation: a particle-based and probabilistic microphysics model coupled with a non-hydrostatic model. Q.J.R. Meteorol. Soc., 135: 1307-1320. https://doi.org/10.1002/qj.441
- Shima, S., Sato, Y., Hashimoto, A., and Misumi, R.: Predicting the morphology of ice particles in deep convection using the super-droplet method: development and evaluation of SCALE-SDM 0.2.5-2.2.0, -2.2.1, and -2.2.2, Geosci. Model Dev., 13, 4107–4157, https://doi.org/10.5194/gmd-13-4107-2020, 2020.