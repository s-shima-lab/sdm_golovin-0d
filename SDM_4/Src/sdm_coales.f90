module m_sdm_coales
  use m_common, only: r8,i8,CONST_RHOW
  use m_rng_uniform_mt

! Implicit typing

  implicit none

! Default access control

  private

! Exceptional access control

  public :: sdm_coales
  
contains

!***********************************************************************
  subroutine sdm_coales(para_b,dvol,dtime,sd_num_0,      &
       &                sd_active,sd_n,sd_mass,          &
       &                active_list,sd_rng,sd_rand)
!***********************************************************************

! Input variables
    real(kind=r8), intent(in) :: para_b    ! [/s] Golovin kernel parameter
    real(kind=r8), intent(in) :: dtime ! tims step of {stochastic coalescence} process
    integer, intent(in) :: sd_num_0        ! initial number of super-droplets
    real(kind=r8), intent(in) :: dvol ! volume of the grid

! Input and output variables
    type(c_rng_uniform_mt), intent(inout) :: sd_rng     ! random number generator
    real(kind=r8), intent(inout) :: sd_rand(1:sd_num_0) ! random numbers
    logical, intent(inout) :: sd_active(1:sd_num_0)     ! .true. if the super-droplet is active
    integer(kind=i8), intent(inout) :: sd_n(1:sd_num_0) ! multiplicity of super-droplets
    real(kind=r8), intent(inout) :: sd_mass(1:sd_num_0) ! mass of super-droplets
    
! Output variables
    integer, intent(out) :: active_list(1:sd_num_0)     ! list storing the index of active super-droplets

! Internal variables
    integer, save :: sd_num_t  ! number of currently acitve super-droplets

    real(kind=r8) :: sd_mtc,sd_mtp ! mass of super-droplets
    real(kind=r8) :: sd_m1       ! mass of super-droplets
                                 ! with large multiplicity
    real(kind=r8) :: sd_m2       ! mass of super-droplets
                                 ! with small multiplicity
    integer(kind=i8) :: sd_n1    ! multiplicity of super-droplets
                                 ! with large multiplicity
    integer(kind=i8) :: sd_n2    ! multiplicity of super-droplets 
                                 ! with small multiplicity
    logical       :: sd_act1     ! active flag of super-droplet
                                 ! with large multiplicity

    real(kind=r8) :: ivvol   ! inverse of a grid volume
    real(kind=r8) :: cc_rate ! collision-coalescence rate 
    real(kind=r8) :: cc_prob ! collision-coalescence probability
    integer(kind=i8) :: sd_nmax  ! maximum multiplicity

    integer :: n             ! index
    integer :: tc            ! index of child droplets
    integer :: tp            ! index of parent droplets
    integer :: cnt           ! temporary

!-----7--------------------------------------------------------------7--

! Check active super-droplets.
    
    cnt=0
    do n=1,sd_num_0

       if( .not. sd_active(n) ) cycle

       cnt = cnt + 1
       active_list(cnt) = n

    end do
    sd_num_t=cnt

    if(sd_num_t<=1) return

! -----

! Collision-coalescence
    ivvol = 1.0_r8/dvol

    do tc=1,sd_num_t
    ! Get random number for collision-coalescence
    call gen_rand_array( sd_rng, sd_rand(tc+1:sd_num_t) )
    do tp=tc+1,sd_num_t

       sd_mtc = sd_mass(active_list(tc))
       sd_mtp = sd_mass(active_list(tp))

       !### Golovin's kernel ###!

       cc_rate = para_b * (sd_mtc + sd_mtp) / CONST_RHOW 
            
       !### Collision-coalescence probability of real-droplets ###!

       cc_prob = cc_rate * dtime * ivvol
       
       !### Collision-coalescence probability of super-droplets ###!
       ! maximum multiplicity
       sd_nmax  = max( sd_n(active_list(tc)), sd_n(active_list(tp)) )

       cc_prob = cc_prob * real( sd_nmax, kind=r8 )

       !### determine if coalescence occurs ###!

       if( sd_rand(tp) >= cc_prob ) cycle  !! no coalesecense

       !### copy the states to temporary variable ###!
       
       if( sd_n(active_list(tc)) > sd_n(active_list(tp)) ) then

          sd_n1  = sd_n( active_list(tc) )
          sd_m1  = sd_mass( active_list(tc) )
          sd_act1= sd_active( active_list(tc) )

          sd_n2  = sd_n( active_list(tp) )
          sd_m2  = sd_mass( active_list(tp) )

       else

          sd_n1  = sd_n( active_list(tp) )
          sd_m1  = sd_mass( active_list(tp) )
          sd_act1= sd_active( active_list(tp) )

          sd_n2  = sd_n( active_list(tc) )
          sd_m2  = sd_mass( active_list(tc) )

       end if

       !### coalescence outcome ###!

       if( sd_n1 > sd_n2 ) then

          sd_n1 = sd_n1 - sd_n2
          sd_m2 = sd_m1 + sd_m2

       else

          sd_n1 = int( sd_n2/2, kind=i8 )
          sd_n2 = sd_n2 - sd_n1

          sd_m1 = sd_m1+sd_m2
          sd_m2 = sd_m1

          !! deactivate if multiplicity=0
          if( sd_n1==0 ) then
             sd_act1= .false.
          end if

       end if

       !### update the super-droplets ###!

       if( sd_n(active_list(tc)) > sd_n(active_list(tp)) ) then

          sd_n( active_list(tc) )  = sd_n1
          sd_mass( active_list(tc) )  = sd_m1
          sd_active( active_list(tc) ) = sd_act1

          sd_n( active_list(tp) )  = sd_n2
          sd_mass( active_list(tp) )  = sd_m2

       else

          sd_n( active_list(tp) )  = sd_n1
          sd_mass( active_list(tp) )  = sd_m1
          sd_active( active_list(tp) ) = sd_act1

          sd_n( active_list(tc) )  = sd_n2
          sd_mass( active_list(tc) )  = sd_m2

       end if

    end do
    end do

! -----

    end subroutine sdm_coales

!-----7--------------------------------------------------------------7--

  end module m_sdm_coales
  
