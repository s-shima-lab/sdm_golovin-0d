module m_common

  implicit none

  public

  integer, parameter :: i8=selected_int_kind(18)
                       ! Kind of double precision integer variable
  integer, parameter :: r8=selected_real_kind(15)
                       ! Kind of double precision real variable

  real(kind=r8), parameter :: CONST_PI = 4.0_r8 * atan( 1.0_r8 )  

  real(kind=r8), parameter :: CONST_RHOW = 1.0e3_r8 ! [kg/m^3] density of liquid water

end module m_common
