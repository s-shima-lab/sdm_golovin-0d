module m_sdm_coales
  use m_common, only: r8,i8,CONST_RHOW
  use m_rng_uniform_mt

! Implicit typing

  implicit none

! Default access control

  private

! Exceptional access control

  public :: sdm_coales
  
contains

!***********************************************************************
  subroutine sdm_coales(para_b,dvol,dtime,sd_num_0,      &
       &                sd_active,sd_mass,               &
       &                active_list,sd_rng,sd_rand)
!***********************************************************************

! Input variables
    real(kind=r8), intent(in) :: para_b    ! [/s] Golovin kernel parameter
    real(kind=r8), intent(in) :: dtime ! tims step of {stochastic coalescence} process
    integer, intent(in) :: sd_num_0        ! initial number of super-droplets
    real(kind=r8), intent(in) :: dvol ! volume of the grid

! Input and output variables
    type(c_rng_uniform_mt), intent(inout) :: sd_rng     ! random number generator
    real(kind=r8), intent(inout) :: sd_rand(1:sd_num_0) ! random numbers
    logical, intent(inout) :: sd_active(1:sd_num_0)     ! .true. if the super-droplet is active
    real(kind=r8), intent(inout) :: sd_mass(1:sd_num_0) ! mass of super-droplets
    
! Output variables
    integer, intent(out) :: active_list(1:sd_num_0)     ! list storing the index of active super-droplets

! Internal variables
    integer, save :: sd_num_t  ! number of currently acitve super-droplets

    real(kind=r8) :: sd_mtc,sd_mtp ! mass of super-droplets
    real(kind=r8) :: sd_m1       ! mass of super-droplets
                                 ! with large multiplicity
    real(kind=r8) :: sd_m2       ! mass of super-droplets
                                 ! with small multiplicity
    logical       :: sd_act1     ! active flag of super-droplet
                                 ! with large multiplicity

    real(kind=r8) :: ivvol   ! inverse of a grid volume
    real(kind=r8) :: cc_rate ! collision-coalescence rate 
    real(kind=r8) :: cc_prob ! collision-coalescence probability

    integer :: n             ! index
    integer :: tc            ! index of child droplets
    integer :: tp            ! index of parent droplets
    integer :: cnt           ! temporary

!-----7--------------------------------------------------------------7--

! Check active super-droplets.
    
    cnt=0
    do n=1,sd_num_0

       if( .not. sd_active(n) ) cycle

       cnt = cnt + 1
       active_list(cnt) = n

    end do
    sd_num_t=cnt

    if(sd_num_t<=1) return

! -----

! Collision-coalescence
    ivvol = 1.0_r8/dvol

    do tc=1,sd_num_t
    ! Get random number for collision-coalescence
    call gen_rand_array( sd_rng, sd_rand(tc+1:sd_num_t) )
    do tp=tc+1,sd_num_t

       !### check if they are still active ###!

       if( (.not. sd_active( active_list(tc) )) .or. (.not. sd_active( active_list(tp) )) ) cycle  

       !### Golovin's kernel ###!

       sd_mtc = sd_mass(active_list(tc))
       sd_mtp = sd_mass(active_list(tp))

       cc_rate = para_b * (sd_mtc + sd_mtp) / CONST_RHOW 
            
       !### Collision-coalescence probability of real-droplets ###!

       cc_prob = cc_rate * dtime * ivvol
       
       !### determine if coalescence occurs ###!

       if( sd_rand(tp) >= cc_prob ) cycle  !! no coalesecense

       !### copy the states to temporary variable ###!
       
       sd_m1  = sd_mass( active_list(tp) )
       sd_act1= sd_active( active_list(tp) )
       sd_m2  = sd_mass( active_list(tc) )

       !### coalescence outcome ###!

       sd_m2 = sd_m1+sd_m2
       sd_act1= .false.

       !### update the super-droplets ###!

       sd_active( active_list(tp) ) = sd_act1
       sd_mass( active_list(tc) )  = sd_m2

    end do
    end do

! -----

    end subroutine sdm_coales

!-----7--------------------------------------------------------------7--

  end module m_sdm_coales
  
