module m_sdm_coales
!$ use omp_lib
  use m_common, only: r8,i8,CONST_RHOW
  use m_rng_uniform_mt

! Implicit typing

  implicit none

! Default access control

  private

! Exceptional access control

  public :: sdm_coales
  
contains

!***********************************************************************
  subroutine sdm_coales(para_b,dvol,dtime,sd_num_0,      &
       &                sd_active,sd_n,sd_mass,          &
       &                active_list,sd_rng,sd_rand)
!***********************************************************************

! Input variables
    real(kind=r8), intent(in) :: para_b    ! [/s] Golovin kernel parameter
    real(kind=r8), intent(in) :: dtime ! tims step of {stochastic coalescence} process
    integer, intent(in) :: sd_num_0        ! initial number of super-droplets
    real(kind=r8), intent(in) :: dvol ! volume of the grid

! Input and output variables
    type(c_rng_uniform_mt), intent(inout) :: sd_rng     ! random number generator
    real(kind=r8), intent(inout) :: sd_rand(1:sd_num_0) ! random numbers
    logical, intent(inout) :: sd_active(1:sd_num_0)     ! .true. if the super-droplet is active
    integer(kind=i8), intent(inout) :: sd_n(1:sd_num_0) ! multiplicity of super-droplets
    real(kind=r8), intent(inout) :: sd_mass(1:sd_num_0) ! mass of super-droplets
    
! Output variables
    integer, intent(out) :: active_list(1:sd_num_0)     ! list storing the index of active super-droplets

! Internal variables
    integer, save :: sd_num_t  ! number of currently acitve super-droplets
    integer :: sd_num_t_half   ! sd_num_t/2

    real(kind=r8) :: sd_mtc,sd_mtp ! mass of super-droplets
    real(kind=r8) :: sd_m1       ! mass of super-droplets
                                 ! with large multiplicity
    real(kind=r8) :: sd_m2       ! mass of super-droplets
                                 ! with small multiplicity
    integer(kind=i8) :: sd_n1    ! multiplicity of super-droplets
                                 ! with large multiplicity
    integer(kind=i8) :: sd_n2    ! multiplicity of super-droplets 
                                 ! with small multiplicity
    logical       :: sd_act1     ! active flag of super-droplet
                                 ! with large multiplicity

    integer :: upscale_factor   ! upscaling factor for collision-coalescence probability
    real(kind=r8) :: frac    ! fraction parts
    real(kind=r8) :: ivvol   ! inverse of a grid volume
    real(kind=r8) :: cc_rate ! collision-coalescence rate 
    real(kind=r8) :: cc_prob ! collision-coalescence probability
    integer(kind=i8) :: sd_nmax  ! maximum multiplicity
    integer(kind=i8) :: sd_ncol ! how many times coalescence occurs

    integer :: tmp_id        ! index
    integer :: n             ! index
    integer :: tc            ! index of child droplets
    integer :: tp            ! index of parent droplets
    integer :: ss            ! temporary
    integer :: cnt           ! temporary

!-----7--------------------------------------------------------------7--

! Check active super-droplets.
    
!$omp single
    cnt=0
    do n=1,sd_num_0

       if( .not. sd_active(n) ) cycle

       cnt = cnt + 1
       active_list(cnt) = n

    end do
    sd_num_t=cnt
!$omp end single
!$omp barrier

    if(sd_num_t<=1) return
    sd_num_t_half = sd_num_t/2

! -----

! Randomly shuffle active_list

    ! Get random number using random number generator
!$omp single
    call gen_rand_array( sd_rng, sd_rand(1:sd_num_t) )
    
    !n = 1
    !active_list(n)= active_list(1)

    do n=2,sd_num_t

       ss = int(sd_rand(n)*n) + 1

       !### swap data ###!
       tmp_id = active_list(n)
       active_list(n) = active_list(ss)
       active_list(ss) = tmp_id

    end do

! -----

! Get random number for collision-coalescence

    call gen_rand_array( sd_rng, sd_rand(1:sd_num_t_half) )
!$omp end single
!$omp barrier

! -----

! Collision-coalescence
    ivvol = 1.0_r8/dvol

!$omp do private(n,tc,tp,sd_mtc,sd_mtp,cc_rate,cc_prob,sd_nmax,upscale_factor, &
!$omp       &    sd_ncol,frac,sd_n1,sd_m1,sd_act1,sd_n2,sd_m2)
    do n=1,sd_num_t_half

       tc = n
       tp = n + sd_num_t_half

       sd_mtc = sd_mass(active_list(tc))
       sd_mtp = sd_mass(active_list(tp))

       !### Golovin's kernel ###!

       cc_rate = para_b * (sd_mtc + sd_mtp) / CONST_RHOW 
            
       !### Collision-coalescence probability of real-droplets ###!

       cc_prob = cc_rate * dtime * ivvol
       
       !### Collision-coalescence probability of super-droplets ###!
       ! maximum multiplicity
       sd_nmax  = max( sd_n(active_list(tc)), sd_n(active_list(tp)) )

       upscale_factor = sd_num_t - 1 + iand(sd_num_t,1)
                               ! IAND(i,1) => even:0, odd:1

       cc_prob = cc_prob * real( sd_nmax*upscale_factor, kind=r8 )

       !### set coalescence count ###!

       sd_ncol = int( cc_prob, kind=i8 )
       frac = cc_prob - real( sd_ncol, kind=r8 )

       ! randomly determine how many times they coalesce

       if( sd_rand(tc) < frac ) then
          sd_ncol =  sd_ncol + 1
       end if

       if( sd_ncol<=0 ) cycle  !! no coalesecense

       !### copy the states to temporary variable ###!
       
       if( sd_n(active_list(tc)) > sd_n(active_list(tp)) ) then

          sd_n1  = sd_n( active_list(tc) )
          sd_m1  = sd_mass( active_list(tc) )
          sd_act1= sd_active( active_list(tc) )

          sd_n2  = sd_n( active_list(tp) )
          sd_m2  = sd_mass( active_list(tp) )

       else

          sd_n1  = sd_n( active_list(tp) )
          sd_m1  = sd_mass( active_list(tp) )
          sd_act1= sd_active( active_list(tp) )

          sd_n2  = sd_n( active_list(tc) )
          sd_m2  = sd_mass( active_list(tc) )

       end if

       !### coalescence outcome ###!

       sd_ncol = min( sd_ncol, int(sd_n1/sd_n2,kind=i8) )

       if( sd_n1 > sd_n2*sd_ncol ) then

          sd_n1 = sd_n1 - sd_n2*sd_ncol
          sd_m2 = sd_m1*real(sd_ncol,kind=r8)+sd_m2

       else

          sd_n1 = int( sd_n2/2, kind=i8 )
          sd_n2 = sd_n2 - sd_n1

          sd_m1 = sd_m1*real(sd_ncol,kind=r8)+sd_m2
          sd_m2 = sd_m1

          !! deactivate if multiplicity=0
          if( sd_n1==0 ) then
             sd_act1= .false.
          end if

       end if

       !### update the super-droplets ###!

       if( sd_n(active_list(tc)) > sd_n(active_list(tp)) ) then

          sd_n( active_list(tc) )  = sd_n1
          sd_mass( active_list(tc) )  = sd_m1
          sd_active( active_list(tc) ) = sd_act1

          sd_n( active_list(tp) )  = sd_n2
          sd_mass( active_list(tp) )  = sd_m2

       else

          sd_n( active_list(tp) )  = sd_n1
          sd_mass( active_list(tp) )  = sd_m1
          sd_active( active_list(tp) ) = sd_act1

          sd_n( active_list(tc) )  = sd_n2
          sd_mass( active_list(tc) )  = sd_m2

       end if

    end do
!$omp end do

! -----

    end subroutine sdm_coales

!-----7--------------------------------------------------------------7--

  end module m_sdm_coales
  
