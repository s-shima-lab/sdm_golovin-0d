program sdm_test_golovin
!$ use omp_lib
  use m_common
  use m_rng_uniform_mt
  use m_sdm_coales
  
  implicit none
 
  integer, parameter       :: sd_num_0   = 2**13       ! []     initial super-droplet number
  real(kind=r8), parameter :: num_conc_0 = 2.0_r8**23  ! [/m^3] initial number concentration of real droplets
  real(kind=r8), parameter :: lwc        = 1.0e-3_r8   ! [kg/m^3] liquid water content
  real(kind=r8), parameter :: dvol       = 1.0e6_r8    ! [m^3] volume of the region
  real(kind=r8), parameter :: dtime      = 1.0_r8      ! [s] time step
  real(kind=r8), parameter :: para_b     = 1500.0e0_r8 ! [/s] Golovin kernel parameter
  real(kind=r8), parameter :: time_end   = 3600.0_r8   ! [s] end time of simulation 
  real(kind=r8), parameter :: save_dtime = 1200.0_r8   ! [s] time interval for data saving
  real(kind=r8), parameter :: r_min =  10.0e-6_r8 ! [m] range of uniform sampling
  real(kind=r8), parameter :: r_max = 100.0e-6_r8 ! [m] range of uniform sampling

  type(c_rng_uniform_mt)              :: sd_rng       ! random number generator
  integer                             :: randseed     ! random number seed
  real(kind=r8), allocatable, save    :: sd_rand(:)   ! random numbers

  logical, allocatable, save          :: sd_active(:) ! .true. if the super-droplet is active
  integer(kind=i8), allocatable, save :: sd_n(:)      ! multiplicity of super-droplets
  real(kind=r8), allocatable, save    :: sd_mass(:)   ! mass of super-droplets

  integer, allocatable, save          :: active_list(:)  ! list of active super-droplets

  real(kind=r8) :: time ! simulation time [s]

  real(kind=r8) :: tmp_mass, tmp_mul, m0, max_multiplicity_real
  real(kind=r8) :: mass_min, mass_max, mass_total
  integer :: n, dec_digit_diff
  integer :: nt, total_nt, save_dnt 
  
  character(len=80) :: fname   ! output filename
  integer, parameter :: FILE_LOG = 10
  integer, parameter :: FILE_DAT = 11

  real(kind=r8) :: dts,dte

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

  !######### allocate arrays
  allocate(sd_rand(1:sd_num_0))  
  allocate(sd_active(1:sd_num_0))  
  allocate(sd_n(1:sd_num_0))  
  allocate(sd_mass(1:sd_num_0))  
  allocate(active_list(1:sd_num_0))  

  !######### initialize time
  time = 0.0_r8
  nt = 0
  total_nt = nint((time_end-time)/dtime)
  save_dnt = nint(save_dtime/dtime)

  !######### initialize random number generator
  ! randseed = 220309 ! for fixed random seed
  call system_clock(count=randseed) ! for automatically generating random seed 
  call rng_init( sd_rng, randseed )
  
  !######### initialize super-droplets
  !!! exponential distribution of droplet mass
  !!! i.e. n(m) = (n0/m0)*exp(-m/m0)
  !!! uniform sampling in log(radius) space

  m0=lwc/num_conc_0

  !### active flag ###!
  sd_active(1:sd_num_0)=.true.

  !### mass ###!
  call gen_rand_array( sd_rng, sd_rand(1:sd_num_0) )
  mass_min=CONST_RHOW*(CONST_PI*4.0_r8/3.0_r8)*r_min**3 
  mass_max=CONST_RHOW*(CONST_PI*4.0_r8/3.0_r8)*r_max**3 
  do n=1,sd_num_0
     sd_mass(n) = exp(log(mass_min)+(log(mass_max)-log(mass_min))*sd_rand(n))
  end do

  !### muliplicity ###!
  call gen_rand_array( sd_rng, sd_rand(1:sd_num_0) )
  dec_digit_diff = max(range(1_i8)-precision(1.0_r8),0)
  max_multiplicity_real = real(huge(1_i8)-10_i8**dec_digit_diff,kind=r8)
  do n=1,sd_num_0
     tmp_mass = sd_mass(n)
     tmp_mul  = log(mass_max/mass_min) &
              & / (real(sd_num_0,kind=r8)/dvol) &
              & * tmp_mass*(num_conc_0/m0)*exp(-(tmp_mass/m0)) ! n(lnm)=m n(m)
     if(tmp_mul .gt. max_multiplicity_real) then
        write(*,*) "too huge multiplicity. exeeds long integer limit"
        stop
     end if

     if(tmp_mul>=1.0_r8)then
        sd_n(n) = nint(tmp_mul,kind=i8)
     else ! sample rare droplet
        if( sd_rand(n) < tmp_mul  )then
           sd_n(n) = 1
        else
           sd_n(n) = 0
           sd_active(n) = .false.
        end if
     end if
  end do

  !### adjustment of total mass ###!
  mass_total=0.0_r8
  do n=1,sd_num_0
     mass_total=mass_total+sd_mass(n)*sd_n(n)
  end do
  sd_mass(:)=sd_mass(:)*lwc*dvol/mass_total
  
  !#########  Start writing log file
  write(fname,'(a)')"log.txt"
  open(FILE_LOG,file=trim(fname),form='formatted',status='replace',action='write')
  write(FILE_LOG,'(a,E,a)')"num_conc_0 = ",num_conc_0," ! [/m^3] initial number concentration of real droplets"
  write(FILE_LOG,'(a,E,a)')"lwc = ",lwc," ! [kg/m^3] liquid water content"
  write(FILE_LOG,'(a,E,a)')"dvol = ",dvol," ! [kg/m^3] liquid water content"
  write(FILE_LOG,'(a,E,a)')"dtime = ",dtime," ! [s] time step"
  write(FILE_LOG,'(a,E,a)')"para_b = ",para_b," ! [/s] Golovin kernel parameter"
  write(FILE_LOG,*)
  write(FILE_LOG,'(a)')"Start of the simulation"

  !#########  Save initial data
  write(fname,'("data_",i5.5,".txt")')int(time)
  open(FILE_DAT,file=trim(fname),form='formatted',status='replace',action='write')

  write(FILE_DAT,'(a)') 'mass[kg] multiplicity[-] status[-] index'
  do n=1,sd_num_0
     write(FILE_DAT,'(E,I,L,I)')sd_mass(n),sd_n(n),sd_active(n),n
  end do
  
  close(FILE_DAT)

  !######### record the start time
!$ dts = omp_get_wtime()

  !#########  Time evolution
  do nt=1,total_nt

     call sdm_coales(para_b,dvol,dtime,sd_num_0,    &
       &                sd_active,sd_n,sd_mass,          &
       &                active_list,sd_rng,sd_rand)

     time = time + dtime
     
     ! save data
     if(mod(nt,save_dnt)==0) then
        write(FILE_LOG,'("Data saved at time =",F8.2,"[s] (",i5,"/",i5," finished)")')time,nt,total_nt
        write(fname,'("data_",i5.5,".txt")')int(time)
        open(FILE_DAT,file=trim(fname),form='formatted',status='replace',action='write')

        write(FILE_DAT,'(a)') 'mass[kg] multiplicity[-] status[-] index'
        do n=1,sd_num_0
           write(FILE_DAT,'(E,I,L,I)')sd_mass(n),sd_n(n),sd_active(n),n
        end do
     
        close(FILE_DAT)
     end if
      
  end do   

  !######### record the end time
!$ dte = omp_get_wtime()

  !#########  Close the log file
  write(FILE_LOG,'(A)')"End of the simulation"
!$ write(FILE_LOG,*)
!$ write(FILE_LOG,'(A,F10.4,A)')"Elapsed time: ",(dte-dts), " s"
  close(FILE_LOG)

  !#########  Deallocate arrays
  deallocate(sd_rand,sd_active,sd_n,sd_mass,active_list)

end program sdm_test_golovin
